# to build ipager manually on arch linux

Currently ipager does not successfully compile and install on arch linux. This explans how to do it yourself manually.

1. make sure the following packages are installed

```
    yaourt -S imlib2 libxmu patch scons
```

2. create a tmp directory for building

```
    mkdir build; cd build
```
    
3. unpack the ipager tgz file into the build directory created in step 2

```
    tar xvzf <ipager-directory>/ipager-1.1.0.tar.gz
```

4. apply the patches

```
    patch -p0 < <ipager-directory>/ipager-1.1.0-20120429.patch
    patch -p0 < <ipager-directory>/ipager.mdp.patch
```
    
5. build the package

```
    scons
```

 this should create a binary called ipager
    
6. install the binary wherever you think best (i.e.  ~/bin)

```
    cp ipager ~/bin/
```
    
7. put `ipager &` in your .fluxbox/startup script somewhere prior to the final exec call to start fluxbox

```
    ipager &
    exec fluxbox
```
